#!/bin/bash /usr/scripts/python.sh

import sys
import math
import itertools

def booklet_order(n):
    page = (lambda k: None if k > n else k)

    recto = 2*int(math.ceil(n/4.))
    verso = recto + 2

    while recto > 1:
        yield page(verso)
        yield page(recto-1)
        yield page(recto)
        yield page(verso-1)
        recto -= 2
        verso += 2

def pdfjam_order(n):
    return ",".join(itertools.imap(lambda k: str(k or '{}'), booklet_order(n)))

if __name__ == '__main__':
    try:
        n = int(sys.argv[1])
    except:
        print "Veuillez entrer un nombre"
        exit(1)

    print pdfjam_order(n)
