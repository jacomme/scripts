#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..
#                       ....  ............   ........
#                     .     .......   .            ....  ..
#                   .  ... ..   ..   ..    ..   ..... .  ..
#                   .. .. ....@@@.  ..  .       ........  .
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    ..
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  .....
#    ...@@@.... @@@    .@@.......... ........ .....        ..
#   . ..@@@@.. .         .@@@@.   .. .......  . .............
#  .   ..   ....           ..     .. . ... ....
# .    .       ....   ............. .. ...
# ..  ..  ...   ........ ...      ...
#  ................................
#
# #############################################################
"""
exceptions.py
 
    Fonctions pour gérer les exceptions
     
Copyright (c) 2006 by www.crans.org
Some code, from cherrypy lib
"""

import sys, traceback


def formatExc(exc=None):
    """formatExc(exc=None) -> exc (or sys.exc_info if None), formatted."""
    if exc is None:
        exc = sys.exc_info()
        
    if exc == (None, None, None):
        return ""
        
    page_handler_str = ""
    args = list(getattr(exc[1], "args", []))
    if args:
        if len(args) > 1:
            page_handler = args.pop()
            page_handler_str = 'Page handler: %s\n' % repr(page_handler)
            exc[1].args = tuple(args)
    return page_handler_str + "".join(traceback.format_exception(*exc))

                
