#!/usr/bin/env python
# -*- coding: utf-8 -*-
# #############################################################
#                                            ..                
#                       ....  ............   ........          
#                     .     .......   .            ....  ..    
#                   .  ... ..   ..   ..    ..   ..... .  ..    
#                   .. .. ....@@@.  ..  .       ........  .    
#              ..  .  .. ..@.@@..@@.  .@@@@@@@   @@@@@@. ....  
#         .@@@@. .@@@@. .@@@@..@@.@@..@@@..@@@..@@@@.... ....  
#       @@@@... .@@@.. @@ @@  .@..@@..@@...@@@.  .@@@@@.    .. 
#     .@@@..  . @@@.   @@.@@..@@.@@..@@@   @@ .@@@@@@..  ..... 
#    ...@@@.... @@@    .@@.......... ........ .....        ..  
#   . ..@@@@.. .         .@@@@.   .. .......  . .............  
#  .   ..   ....           ..     .. . ... ....                
# .    .       ....   ............. .. ...                     
# ..  ..  ...   ........ ...      ...                          
#  ................................                            
#                                                              
# #############################################################
# models.py
#
#     Classe de base pour mapper automatiquement des
#     enregistrements ldap et des objets python
#
# Auteur: Grégoire Détrez <gdetrez@crans.org>
# Copyright (c) 2008 by www.crans.org
# #############################################################
import fields

class MetaModel(type):
    def __init__(cls, name, bases, attrs):
        super(type, cls).__init__(cls, name, bases, attrs)
        _fields = {}
        for item_name in attrs:
             if isinstance(attrs[item_name], fields.baseField):
                 _fields[item_name] = attrs[item_name]
        setattr(cls, "_model_fields", _fields)


class Model(object):
    __metaclass__ = MetaModel
    def __init__( self, data ):
        k = self._model_fields
        for a_field in self._model_fields:
            val = self._model_fields[a_field].load(data.get(a_field, None))
            setattr(self, a_field, val)

