#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
Génère un graphe de dépendance des extensions 
du plan de numérotation d'asterisk

Auteur: Valentin Samir

"""

from subprocess import Popen, PIPE, STDOUT
import time

def gen_extentions_graph(exten_file, start_nodes, svg_file):

    out=""
    arrow={}
    iarrow={}

    out+="""digraph G {
    rankdir=LR
    edge [len=2.50, ratio=fill]
    """
    for node in start_nodes:
        out+='"%s" [style=filled];' % node

    file=open(exten_file).read()
    file=file.split('\n[')
    del(file[0])
    for context in file:
        context=context.split('\n')
        context_name = context[0].strip()[:-1]
        arrow[context_name]= arrow.get(context_name, set())
        iarrow[context_name]= iarrow.get(context_name, set())
        del(context[0])
        out+='"%s";' % context_name
        for line in context:
            if line.startswith('include'):
                dest=line.split('=>', 1)[1].strip()
                iarrow[context_name].add(dest)
            line=line.split(',')
            if len(line)>2 and line[2].strip().startswith('Goto('):
                dest=line[2].strip()[5:]
                arrow[context_name].add(dest)


    for (context_name, dests) in arrow.items():
        for dest in dests:
            out+='"%s" -> "%s" [arrowhead=normal];' % (context_name, dest)
    for (context_name, dests) in iarrow.items():
        for dest in dests:
            out+='"%s" -> "%s" [arrowhead=crow, style=dashed];' % (context_name, dest)

    out+="""
      node [shape=plaintext]
      subgraph cluster_01 {
        key [label=<<table border="0" cellpadding="2" cellspacing="0" cellborder="0">
          <tr><td align="right" port="i1">B inclus dans A : A</td></tr>
          <tr><td align="right" port="i2">Saut de A vers B :  A</td></tr>
          </table>>];
        key2 [label=<<table border="0" cellpadding="2" cellspacing="0" cellborder="0">
          <tr><td port="j1">B</td></tr>
          <tr><td port="j2">B</td></tr>
          </table>>];
        key:i1:e -> key2:j1:e [style=dashed, arrowhead=crow]
        key:i2:e -> key2:j2:w []
      }
    """

    out+="""
    label = "\\n\\nAsterisk extensions\\nLes Nounous\\n%s";
    }""" % time.strftime('%A %d %B %Y, %H:%M:%S')


    p = Popen(['dot', '-Tsvg', '-o', svg_file], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    p.communicate(input=out)

if __name__ == '__main__' :
    import sys
    if len(sys.argv)>1:
        import locale
        locale.setlocale(locale.LC_TIME, 'fr_FR.UTF-8')
        gen_extentions_graph('/etc/asterisk/extensions.conf', ["crans-sip", "crans-from-external", "ovh-sip", "crans-sms"], sys.argv[1])
