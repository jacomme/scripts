#!/usr/bin/python
# -*- coding: utf8 -*-
import urllib
from config import *

i=0
for group in multicast.keys():
	for (title, (name,dst,port,sources)) in multicast[group].items():
		#name="channel%s" % i
		print('new %s broadcast enabled loop' % name)
#		for source in sources:
#			print('setup %s input "%s"' % (name,source))
		print('setup %s input "%s"' % (name,'http://tv.crans.org:8000/%s' % name))
		print('setup %s output #udp{mux=ts,dst=%s:%s}' % (name,dst,port))
		print('setup %s option network-caching=50' % name)
		print('setup %s option sout-all' % name)
		print('setup %s option sout-keep' % name)
		#print('control %s play' % name)
		i+=1
