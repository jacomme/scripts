#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-

import os, sys, socket, shutil
sys.path.append('/usr/scripts/gestion')
import lock
import time
from time import localtime, sleep

tmp_dir = '/tmp'
www_dir = '/var/www/'
img_dir = '%s/images' % www_dir

t = localtime()
trame_entete = """
<!DOCTYPE HTML PUBLIC \"-//W3C//DTD HTML 4.01 Transitional//EN\" \"http://www.w3.org/TR/html4/loose.dtd\">
<html lang=\"fr\">
<head>
<title>Chaines diffus&eacute;es</title>
<meta http-equiv=\"refresh\" content=\"300\">
<META HTTP-EQUIV=\"Content-Type\" CONTENT=\"text/html; charset=iso-8859-1\">
</head>
<body BACKGROUND=\"style2_left.png\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\" STYLE=\"background-repeat:repeat-y\">
<img src=\"style2_top.png\" alt=\"En_tete\"><br>
<div id=\"Titre\" style=\"position:absolute; left:580px; top:70px; width:400px;height:34px; z-index:2\"><font size=\"5\"><b>Chaines diffus&eacute;es</b></font></div>
<div id=\"Heure\" style=\"position:absolute; left:380px; top:140px; width:500px;height:34px; z-index:2\"><font size=\"4\">
<u>Cette page &agrave; &eacute;t&eacute; g&eacute;n&eacute;r&eacute;e &agrave; %02ih%02i</u></font></div>
<div id=\"Texte\" style=\"position:absolute; left:245px; top:190px; right:16px; z-index:1; overflow: visible; visibility: visible; background-color: #FFFFFF; layer-background-color: #FFFFFF;\">
</center>
<h2><b><a href=http://wiki.crans.org/moin.cgi/TvReseau>Pour plus d'informations cliquez ici</a><br></b></h2>
<a href=vignettes_petites.html>Petites vignettes</a><br>
<a href=vignettes.html>Vignettes Normales</a><br>
<table cellpading=25 cellspacing=25>
<tr>
""" % (int(t[3]), int(t[4]) )

table_piedpage = """</tr>
</table>
"""

trame_piedpage = """</body>
</html>
"""

def ajoute_image(nom, fqdn, ip, html, html_petites):
    html.write('<td align="center">')
    html.write('<img src="images/%s.jpg" alt="Chaine : %s / IP : %s"><br>' % (fqdn, nom, ip))
    html.write('<b><u>Chaine :</u></b> %s<br><b><u>IP :</u></b> %s:1234' % (nom, ip))
    html.write('</td>\n')
    html_petites.write('<td align="center">')
    html_petites.write('<img src="images/%s_petites.jpg" alt="Chaine : %s / IP : %s"><br>' % (fqdn, nom, ip))
    html_petites.write('<b><u>Chaine :</u></b> %s<br><b><u>IP :</u></b> %s:1234' % (nom, ip))
    html_petites.write('</td>\n')

def vignettes() :
    html = open('%s/vignettes.html' % tmp_dir,'w')
    html.write(trame_entete)
    html_petites = open('%s/vignettes_petites.html' % tmp_dir,'w')
    html_petites.write(trame_entete)
    col = 0
    col_petites = 0
    #a factoriser
    chaines_probleamatiques = []
    os.system('/usr/scripts/tv/recup_sap')
    #On est oblig� de mettre en dur le path 
    #car il n'est pas configurable dans recup_sap
    chaines_sap = open('/tmp/chaines_recup_sap.txt','r').readlines()
    chaines_sap_sort = open('%s/chaines_recup_sap_triees.txt' % tmp_dir, 'w' )
    chaines_sap.sort()
    for line in chaines_sap :
        chaines_sap_sort.write(line.replace('\'',''))
    chaines_sap_sort.close()

    for line in chaines_sap :
        
        line = line.strip()
        nom = line.split(':')[0]
        ip = line.split(':')[-1]
        print '%s %s' % (str(ip), str(nom))
        try :
            fqdn = socket.gethostbyaddr(ip)[0]
        except socket.herror, err :
            print 'On n\'arrive pas � r�soudre ce nom %s %s' % (ip, err[1])
            continue
        except :
            print 'Erreur inconnue'
            continue
        else :
        #on �vite les radios, peu de debit et pas de vignettes
            if nom[0:3] == 'rad' :
                #C'est une radio on cherche un logo
                nom_court = (' '.join(nom.split(' ')[2:])).lower()
                print "On s'occupe de la radio %s" % nom_court
                for file in os.listdir('%s /logos_radios' % img_dir):
                    try :
                        shutil.copy(' %s/logos_radios/%s' % (img_dir, file),'%s/%s.jpg' % (img_dir, fqdn))
                        shutil.copy(' %s/logos_radios/%s' % (img_dir, file),'%s/%s_petites.jpg' % (img_dir, fqdn))
                    except :
                        #il n'y a pas de logo :-(
                        chaines_probleamatiques.append('<b><u>Chaine :</u></b> %s<br><b><u>IP :</u></b> %s:1234<br><br>\n' % (nom, ip))
                        continue
                    else :
                        if col == 2 :
                            col = 1
                            html.write('</tr><tr>\n')
                        else :
                            col += 1
                        if col_petites ==  3:
                            col_petites = 1
                            html_petites.write('</tr><tr>\n')
                        else :
                            col_petites += 1
                        ajoute_image(nom, fqdn, ip, html, html_petites)
            print 'on s\'occupe de  %s %s' % (ip, nom)
            print '\trecuperation du flux'
            #ip port duree
            os.system('/usr/scripts/tv/dumpudp %s 1234 2 > %s/%s.ts 2>/dev/null' % (ip, tmp_dir, ip))
            pid = os.spawnlp(os.P_NOWAIT,'ffmpeg','ffmpeg','-deinterlace','-an','-i','%s/%s.ts' % (tmp_dir, ip),'-s','400x300','-r','1','-t','00:00:01','-f','image2','%s/%s.jpg' % (img_dir, fqdn))
            time.sleep(3)
            os.kill(pid, 9)
            if not os.path.exists('%s/%s.jpg' % (img_dir, fqdn)) or not os.stat('%s/%s.jpg' % (img_dir, fqdn))[6] :
                print '\tOn retente'
                os.system('/usr/scripts/tv/dumpudp %s 1234 2 > %s/%s.ts ' % (ip, tmp_dir, ip))
                pid = os.spawnlp(os.P_NOWAIT,'ffmpeg','ffmpeg','-deinterlace','-an','-i %s/%s.ts' % (tmp_dir, ip),'-s 400x300','-r 1','-t 00:00:01','-f image2 %s/%s.jpg' % (img_dir, fqdn))
                time.sleep(3)
                os.kill(pid, 9)
            if not os.path.exists('%s/%s.jpg' % (img_dir, fqdn)) or not os.stat('%s/%s.jpg' % (img_dir, fqdn))[6] :
                shutil.copy('%s/fail.jpg' % (img_dir) , '%s/%s.jpg' % (img_dir, fqdn))

            if os.path.exists('%s/%s_petites.jpg' % (img_dir, fqdn)) :
                os.remove('%s/%s_petites.jpg' % (img_dir, fqdn))
            os.system('convert -geometry \'200x150 !\' %s/%s.jpg %s/%s_petites.jpg 2>/dev/null 1>/dev/null' % (img_dir, fqdn, img_dir, fqdn))
            #On met des symlink pour garder une compatibilit� avec les programmes utilisant les adresses ip
            if os.path.exists('%s/%s.jpg' % (img_dir, ip)) :
                os.remove('%s/%s.jpg' % (img_dir, ip))
            if os.path.exists('%s/%s_petites.jpg' % (img_dir, ip)) :
                os.remove('%s/%s_petites.jpg' % (img_dir, ip))
            os.symlink('%s/%s.jpg' % (img_dir, fqdn),'%s/%s.jpg' % (img_dir, ip))
            os.symlink('%s/%s_petites.jpg' % (img_dir, fqdn),'%s/%s_petites.jpg' % (img_dir, ip))
            
            if os.path.exists('%s/%s.ts' % (tmp_dir, ip)) and  os.stat('%s/%s.ts' % (tmp_dir, ip))[6] : #on teste sur le ts car il est enleve a chaque fois
                if col == 2 :
                    col = 1
                    html.write('</tr><tr>\n')
                else :
                    col += 1
                if col_petites ==  3:
                    col_petites = 1
                    html_petites.write('</tr><tr>\n')
                else :
                    col_petites += 1
                ajoute_image(nom, fqdn, ip, html, html_petites)
                print "--------------- Ok %s" % ip
            else :
                chaines_probleamatiques.append('<b><u>Chaine :</u></b> %s<br><b><u>IP :</u></b> %s:1234<br><br>\n' %(nom, ip))
                print "--------------- Problematique %s  ..." % ip
                if os.path.exists('%s/%s.jpg' % (img_dir, fqdn)) :
                    os.remove('%s/%s.jpg' % (img_dir, fqdn))
                if os.path.exists('%s/%s_petites.jpg' % (img_dir, fqdn)) :
                    os.remove('%s/%s_petites.jpg' % (img_dir, fqdn))
        
            if os.path.exists('%s/%s.ts' % (tmp_dir, ip)) :
                os.remove('%s/%s.ts' % (tmp_dir, ip))

    html.write(table_piedpage)
    html_petites.write(table_piedpage)
    #on ecrit les radios
    html.write('<br><u><b><h3>Liste des chaines diffus&eacute;es mais dont le flux est &eacute;rron&eacute; ou uniquement audio (les flux videos ne seront probablement non lisibles avec VLC mais lisibles avec xine)</h3></b></u><br>')
    html_petites.write('<br><u><b><h3>Liste des chaines diffus&eacute;es mais dont le flux est &eacute;rron&eacute; ou uniquement audio (les flux videos ne seront probablement non lisibles avec VLC mais lisibles avec xine)</h3></b></u><br>')
    for line in chaines_probleamatiques :
        html.write(line)
        html_petites.write(line)
        #on ecrit la vraie fin
    html.write(trame_piedpage)
    html.close()
    html_petites.write(trame_piedpage)
    html_petites.close()
    shutil.copy('%s/vignettes.html' % tmp_dir,'%s/vignettes.html' % www_dir)
    shutil.copy('%s/vignettes_petites.html' % tmp_dir,'%s/vignettes_petites.html' % www_dir)


if __name__ == '__main__' :
    lock.make_lock('vignettes')
    print 'Vignetisation g�n�rale'
    vignettes()
    print 'On a fini'
    lock.remove_lock('vignettes')
