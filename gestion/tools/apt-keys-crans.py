#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-

import os
import sys
from subprocess import Popen, PIPE, STDOUT
import lc_ldap.shortcuts
from socket import gethostname

keyserver='komaz.adm.crans.org'
basedir='/bcfg2/Cfg/etc/crans/apt-keys/'

conn=lc_ldap.shortcuts.lc_ldap_readonly()
nounou=conn.search(u"(&(gpgFingerprint=*)(droits=nounou))")
fpr=[u['gpgFingerprint'][0].value for u in nounou]

def refresh_keys():
    p = Popen(['gpg', '--keyserver', 'odlyd.adm.crans.org', '--recv-keys'] + fpr, stdout=PIPE, stdin=PIPE, stderr=STDOUT)
    # ret = (stdoutdata, stderrdata) if stdout/stderr sent to PIPE
    ret = p.communicate()
    if ret[1]:
        print >> sys.stderr, ret[1]

def write_keys():
    for user in nounou:
        try:
            os.mkdir(basedir + '%s.asc/' % user['uid'][0])
        except OSError:
            pass
        path=basedir + '%s.asc/%s.asc' % (user['uid'][0],user['uid'][0])
        # Est-ce que ça serait bien de mettre --export-options export-minimal ?
        p = Popen(['gpg', '--armor', '--export-options', 'export-minimal', '--export', user['gpgFingerprint'][0].value], stdout=PIPE, stdin=PIPE, stderr=STDOUT)
        ret=p.communicate()
        if ret[1]:
            print >> sys.stderr, ret[1]
        if ret[0]:
            with open(path, 'w') as f:
                f.write(ret[0])
                f.close()

if __name__ == '__main__':
    if gethostname() != 'bcfg2':
        print >> sys.stderr, "Doit être lancé sur bcfg2"
        exit(1)
    else:
        refresh_keys()
        write_keys()
