# -*- coding: utf-8 -*-

import sys

in_encoding = getattr(sys.stdin, 'encoding', None) or "UTF-8"
out_encoding = getattr(sys.stdout, 'encoding', None) or "UTF-8"
