#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Variables de configuration pour la gestion du DNS """

import os

# import des variables génériques
import __init__ as config

#: ariane et ariane2 pour la zone parente
parents = [
    '138.231.176.4',
    '138.231.176.54',
]
#: DNS master
master = '10.231.136.9'
#: DNS slaves (ovh et titanic /aka/ freebox)
slaves = [
    '10.231.136.8', # ovh
    '10.231.136.14', # titanic aka freebox
    '10.231.136.108', # soyouz
]
#: DNS master de la zone tv
master_tv = master
#: DNS slaves de la zone tv
slaves_tv = slaves

#: Zone tv
zone_tv = 'tv.crans.org'

#: DNS en connexion de secours
secours_relay = '10.231.136.14';

#: Serveurs autoritaires pour les zones crans, le master doit être le premier
DNSs = [
    'sable.crans.org',
    'freebox.crans.org',
    'soyouz.crans.org',
]

MXs = {
    'redisdead.crans.org': {
        'prio': 10,
    },
    'freebox.crans.org': {
        'prio': 25,
    },
    'soyouz.crans.org': {
        'prio': 15,
    },
}

#: Résolution DNS directe, liste de toutes les zones crans hors reverse
zones_direct = [
    'crans.org',
    'crans.ens-cachan.fr',
    'wifi.crans.org',
    'clubs.ens-cachan.fr',
    'adm.crans.org',
    'crans.eu',
    'wifi.crans.eu',
    'tv.crans.org',
    'ap.crans.org',
]
#: Les zones apparaissant dans des objets lc_ldap
zones_ldap = [
    'crans.org',
    'crans.ens-cachan.fr',
    'wifi.crans.org',
    'clubs.ens-cachan.fr',
    'adm.crans.org',
    'tv.crans.org',
]
#: Zones signée par opendnssec sur le serveur master
zones_dnssec = [
    'crans.org',
    'wifi.crans.org',
    'adm.crans.org',
    'tv.crans.org',
    'crans.eu',
]
#: Zones alias : copie les valeur des enregistrement pour la racine de la zone et utilise un enregistemenr DNAME pour les sous domaines
zone_alias = {
    'crans.org' : [
        'crans.eu',
    ],
}

#: Résolution inverse v4
zones_reverse = config.NETs["all"] + config.NETs["adm"] + config.NETs["personnel-ens"] + config.NETs['multicast']
#: Résolution inverse v6
zones_reverse_v6 = config.prefix['fil'] + config.prefix['wifi'] + config.prefix['adm'] + config.prefix['personnel-ens'] # à modifier aussi dans bind.py

#: Serveurs DNS récursifs :
recursiv = {
    'fil' : [
        '138.231.136.98',
        '138.231.136.152',
    ],
    'wifi' : [
        '138.231.136.98',
        '138.231.136.152',
    ],
    'evenementiel' : [
        '138.231.136.98',
        '138.231.136.152',
    ],
    'adm' : [
        '10.231.136.98',
        '10.231.136.152',
    ],
    'gratuit' : [
        '10.42.0.164',
    ],
    'accueil' : [
        '10.51.0.10',
    ],
    'isolement' : [
        '10.52.0.10',
    ],
    'personnel-ens' : [
        '10.2.9.10',
        '138.231.136.98',
        '138.231.136.152',
    ],
    'federez' : [
        '138.231.136.98',
        '138.231.136.152',
    ],
}

#: Les ip/net des vlans limité vue par les récursifs
menteur_clients = [
    "138.231.136.210",
    "138.231.136.10",
] + config.prefix['evenementiel']

# Chemins de fichiers/dossiers utiles.
DNS_DIR = '/etc/bind/generated/'
DNSSEC_DIR = '/etc/bind/signed/'
# Fichier de définition des zones pour le maître
DNS_CONF = os.path.join(DNS_DIR, 'zones_crans')

# Fichier de définition des zones pour les esclaves géré par BCfg2
DNS_CONF_BCFG2 = "/var/lib/bcfg2/Cfg/etc/bind/generated/zones_crans/zones_crans"
