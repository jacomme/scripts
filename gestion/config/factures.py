#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""Déclaration des items accessibles à la vente (prix coûtant) et générant
une facture.
items est un dictionnaire, dont chaque entrée est composée d'un dictionnaire
ayant une désignation, un prix unitaire, et indique si l'item n'est accessible
qu'aux imprimeurs (par défaut, non)."""

# Les clef sont un code article
ITEMS = {
    'CABLE': {
        'designation': u'Cable Ethernet 5m',
        'pu': 3.,
    },
    'ADAPTATEUR': {
        'designation': u'Adaptateur Ethernet/USB',
        'pu': 17.,
    },
    'RELIURE': {
        'designation': u'Reliure plastique',
        'pu': 0.12,
    },
    'SOLDE': {
        'designation': u'Rechargement du solde',
        'pu': '*',
        'imprimeur': True,
    },
    'PULL_ZIP_MARK': {
        'designation': u'Zipper marqué',
        'pu': 39.18,
    },
    'PULL_ZIP': {
        'designation': u'Zipper non marqué',
        'pu': 35.8,
    },
    'PULL_MARK': {
        'designation': u'Capuche marqué',
        'pu': 32.28,
    },
    'PULL': {
        'designation': u'Capuche non marqué',
        'pu': 28.92,
    },
}
