#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
# Envoi d'un mail donné à certains adherents
# Premier parametre : critere de recherche
# Second parametre, fichier à envoyer

import smtplib, sys, os, ldap_crans, time, socket, commands
from email_tools import format_sender
from affich_tools import cprint

def postconf(i):
    "Fixe la fréquence d'envoi maximale par client (en msg/min)"
    os.system("/usr/sbin/postconf -e smtpd_client_message_rate_limit=%s" % i)
    os.system("/etc/init.d/postfix reload")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        cprint(u"""Usage:

Ce script permet d'envoyer un mail à toute une catégorie d'adhérents.

Le premier paramètre est le critère de recherche :
   paiement=ok pour les adhérents en règle
   paiement=ok&paiement!=2004 pour ceux qui n'ont pas encore payé pour cette année
   chbre=????&paiement=ok pour ceux dont la chambre est inconnue
   paiement=ok&carteEtudiant!=2004 pour ceux qui n'ont pas de carte d'étudiant

Le second paramètre est un fichier texte qui contient le message à envoyer. Il
doit également contenir les entêtes, à l'exception du destinataire qui sera rajouté
par le script.

 /!\ Ce script ne demande aucune confirmation, il faut veiller à
     vérifier avec whos que l'on cible bien les utilisateurs que l'on
     veut. Et si on veut vérifier que le mail a une bonne tête on se
     l'envoie d'abord en mettant login=machin comme critère de
     recherche.

 /!\ Ce script a pour but de spammer, et spammer c'est mal. Il faut
     donc contourner les limitations qui ont été mises en place au
     Cr@ns. Si plus de 10 mails doivent être envoyés, il faut le faire
     depuis redisdead, et en ayant pensé à désactiver la limite de mails
     par minute :
     # postconf -e 'smtpd_client_message_rate_limit = 0'
     # service postfix reload
""")
        sys.exit(0)

    # On en est là
    # On ouvre la base et on cherche
    adherents = ldap_crans.crans_ldap().search(sys.argv[1])['adherent']
    cprint(u"%d adhérent(s) a/ont été trouvé(s)..." % len(adherents))
    time.sleep(3)                       # On dort un peu, ctrl-c welcome

#    opt = commands.getoutput("/usr/sbin/postconf smtpd_client_message_rate_limit")
#    limit = int(opt.split()[-1])
#    if limit > 0 and len(adherents) >= 10:
#        limit_risen = True
#        postconf(0)
#    else: limit_risen = False
    limit_risen = False

    try:
        try:
            fichier = open(sys.argv[2])
            texte = fichier.read()
            fichier.close()
        except IOError:
            cprint(u"Impossible d'ouvrir le fichier à envoyer, merci, au revoir.")
            sys.exit(1)

        echecs = []
        s = smtplib.SMTP()
        s.connect('smtp.crans.org')
        for adherent in adherents:
            mail = adherent.mail().encode("utf-8", "ignore")
            if "@" not in mail:
                mail = mail + "@crans.org"
            cprint(u"Envoi du mail à %s <%s>..." % (adherent.Nom(), mail))
            try:
                recipient = format_sender(u'"%s" <%s>\n' % (adherent.Nom(), mail))
                s.sendmail('bulk+%s@crans.org' % mail.replace("@",'-at-'),
                (mail,),
                "To: %s\n%s" % (recipient, texte))
            except:
                cprint(u"Erreur lors de l'envoi à %s <%s>..." % (adherent.Nom(), mail), "rouge")
                echecs.append(mail)
            else:
                # Tout va bien
                pass

        if echecs:
            print "\nIl y a eu des erreurs :"
            print echecs

        s.close()

    # On rétablit la conf de postfix
    finally:
        if limit_risen: postconf(limit)


