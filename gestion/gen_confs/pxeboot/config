#!/bin/bash -e
#
# pxeboot.sh: création d'un répertoire TFTP de boot par PXE
# Copyright (C) 2008, Nicolas Dandrimont <Nicolas.Dandrimont@crans.org>
#
# Utilisation : pxeboot.sh IP_SRV_TFTP

# Définitions communes
TFTPROOT="/var/lib/tftpboot"
KSROOT="/var/www/pxe"
SKELETON="$TFTPROOT-base"
TMPDIR="/var/tmp/build-netboot"
ISODIR="/var/lib/tftpboot-g"
WGETOPT="-4"
[[ $1 == "" ]] && echo "Il faut spécifier l'IP du serveur en paramètre" && exit 1
OWN_IP="$1"
/etc/init.d/nfs-kernel-server stop

# Définitions spécifiques au Sys Rescue CD
SYSRCCD_ARCHS=""
SYSRCCD_FTP="http://ftp.crans.org/pub/distributions/linux/systemrescuecd"

# Définitions spécifiques à Debian
DEBIAN_DISTS="squeeze wheezy jessie"
DEBIAN_ARCHS="i386 amd64"
DEBIAN_FTP="ftp://ftp.crans.org/debian/dists"

#Image debian custom avec plus de drivers : http://kmuto.jp/debian/d-i/
DEBIAN_BACKPORT_DISTS=""
DEBIAN_BACKPORT_ARCHS="i386 amd64"
DEBIAN_BACKPORT_FTP="ftp://cdimage.debian.org/cdimage/unofficial/backports/"

# Définitions spécifiques à Ubuntu
UBUNTU_DISTS="precise trusty utopic vivid"
UBUNTU_ARCHS="i386 amd64"
UBUNTU_FTP="ftp://ftp.crans.org/ubuntu/dists"

UBUNTU_LIVE="12.04 14.04 14.10 15.04"
# il faut modifier le nfs (ajouter la sortie de export_ubuntu_live
# à /etc/exports) et mettre les images dans $ISODIR/ubuntu/ puis
# les monter dans $TFTPROOT/livecd/ubuntu/$dist-$arch avec
# mount_ubuntu_live
UBUNTU_LIVE_TYPE="ubuntu xubuntu kubuntu"
UBUNTU_LIVE_ARCHS="i386 amd64"

# Définitions spécifiques à Mandriva
MANDRIVA_DISTS=""
MANDRIVA_ARCHS="i586 x86_64"
MANDRIVA_FTP="ftp://ftp.free.fr/mirrors/ftp.mandriva.com/MandrivaLinux/official"

# Définitions spécifiques à CentOS
CENTOS_DISTS="6.5 6.6"
CENTOS_ARCHS="i386 x86_64"
CENTOS_FTP="ftp://mirror.in2p3.fr/pub/linux/CentOS"

# Définitions spécifiques à Fedora
FEDORA_DISTS="20 21 22"
FEDORA_ARCHS="i386 x86_64"
FEDORA_FTP="ftp://ftp.free.fr/mirrors/fedora.redhat.com/fedora/linux/"

# Définitions spécifiques à OpenSuse
OPENSUSE_DISTS="12.2 12.3 13.1 13.2"
OPENSUSE_ARCHS="i386 x86_64"
OPENSUSE_FTP="ftp://ftp.free.fr/mirrors/ftp.opensuse.org/opensuse/distribution/"

# Définitions spécifiques à FreeBSD
FREEBSD_DISTS=""
FREEBSD_ARCHS="i386 amd64"
FREEBSD_FTP="ftp://ftp.fr.freebsd.org/pub/FreeBSD/"

# Définition spécifiques à NetBSD
#~ NETBSD_DIST="5.1 6.0"
#~ NETBSD_ARCHS="i386 amd64"
#~ NETBSD_FTP="ftp://iso.fr.netbsd.org/pub/NetBSD"

# Définitions spécifiques à OpenBSD
OPENBSD_DIST=""
OPENBSD_ARCHS="i386 amd64"
OPENBSD_FTP="ftp://ftp.crans.org/pub/OpenBSD"
