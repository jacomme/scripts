{{ config_header }}

hostname "{{ hostname }}"
; Generated on {{ date_gen }} by switchs2.py
{{ module_type }}
;--- Snmp ---
snmp-server contact "root@crans.org"
snmp-server location "Batiment {{ bat }}"
;A faire à la main
snmpv3 enable
snmpv3 restricted-access
;snmpv3 user "initial"
snmpv3 user "crans"
snmpv3 group ManagerPriv user "crans" sec-model ver3
snmp-server community "public" Operator
;--- Heure/date ---
time timezone 60
time daylight-time-rule Western-Europe
{%- for s in ntp_servers %}
sntp server {{ s }}
{%- endfor %}
timesync sntp
sntp unicast
;--- Misc ---
console inactivity-timer 30
;--- Logs ---
{%- for s in log_servers %}
logging {{ s }}
{%- endfor %}
;--- IP du switch ---
ip default-gateway 10.231.136.4
{%- for vlan in vlans %}
vlan {{ vlan.id }}
   name "{{ vlan.name|capitalize }}"
   {%- if vlan.tagged %}
   tagged {{ vlan.tagged }}
   {%- endif %}
   {%- if vlan.untagged %}
   untagged {{ vlan.untagged }}
   {%- endif %}
   {%- if vlan.ip_cfg %}
   ip address {{ vlan.ip_cfg.0 }} {{ vlan.ip_cfg.1 }}
   {%- else %}
   no ip address
   {%- endif %}
   {%- if vlan.extra %}
   {{ vlan.extra|indent(3, false) }}
   {%- endif %}
exit
{%- endfor %}
;--- Accès d'administration ---
no telnet-server
no web-management
aaa authentication ssh login public-key none
aaa authentication ssh enable public-key none
ip ssh
ip authorized-managers 10.231.136.0 255.255.255.0
ip ssh filetransfer
;--- Protection contre les boucles ---
loop-protect disable-timer 30
loop-protect transmit-interval 3
loop-protect {{ non_trusted }}
;--- Serveurs radius ---
radius-server dead-time 2
radius-server key {{ radius_key }}
{%- for s in radius_servers %}
radius-server host {{ s }}
{%- endfor %}
;--- Filtrage mac ---
aaa port-access mac-based addr-format multi-colon
;--- Bricoles ---
no cdp run
no stack
;--- DHCP Snooping ---
{%- if dhcp_snooping_vlan_names %}
dhcp-snooping vlan{% for n in dhcp_snooping_vlan_names %} {{ n|vlan_id }}{% endfor %}
{%- for s in dhcp_servers %}
dhcp-snooping authorized-server {{ s }}
{%- endfor %}
; Activation
dhcp-snooping
{%- endif %}
{% if ra_filter %};--- RA guards ---
ipv6 ra-guard ports {{ non_trusted }}
no ipv6 ra-guard ports {{ trusted }} 
{% endif %}
;--- Config des prises ---
{%- for port in ports %}
{%- if port.radius_auth() %}
aaa port-access mac-based {{ port|int }}
aaa port-access mac-based {{ port|int }} addr-limit {{ port.num_mac() }}
aaa port-access mac-based {{ port|int }} logoff-period 3600
aaa port-access mac-based {{ port|int }} unauth-vid 1
{%- endif %}
interface {{ port|int }}
   enable
   name "{{ port }}"
   {{ port.flowcontrol() }}
   {%- if port.is_trusted() %}
   dhcp-snooping trust
   {%- endif %}
   {%- if gigabit %}
   {{ port.speed() }}
   {%- endif %}
   no lacp
exit
{%- endfor %}
;--- Filtre de protocole ---
{%- if drop_mdns %}
filter multicast 01005e0000fb drop all
filter multicast 3333000000fb drop all
{%- endif %}
