#!/bin/bash /usr/scripts/python.sh
# -*- coding: utf-8 -*-
#
# Parser for firewall service.
#
# Author  : Pierre-Elliott Bécue <becue@crans.org>
# Licence : GPLv3
# Date    : 15/06/2014
"""
This is the parser for firewall service.
"""

import lc_ldap.attributs as attributs
from gestion.trigger.host import record_parser, chaining

@record_parser(attributs.macAddress, attributs.ipHostNumber)
@chaining(0)
def secours(ob_id, body, diff, more):
    """Computes mac_ip data to send from body and diff

    Body is a couple of two dicts (before, after)

    """
    macs = tuple([body[i].get(lc_ldap.attributs.macAddress.ldap_name, [''])[0] for i in xrange(0, 2)])
    ips = tuple([body[i].get(lc_ldap.attributs.ipHostNumber.ldap_name, [''])[0] for i in xrange(0, 2)])

    # Mise à jour du parefeu mac_ip
    if not macs[0]:
        # Création d'une nouvelle machine.
        fw_dict = {'add': [(macs[1], ips[1])]}
    elif not macs[1]:
        # Destruction d'une machine.
        fw_dict = {'delete': [(macs[0], ips[0])]}
    else:
        # Mise à jour.
        fw_dict = {'update': [(macs[0], ips[0], macs[1], ips[1])]}
    return ("secours", ("mac_ip", fw_dict))

