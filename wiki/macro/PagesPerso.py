# -*- encoding: utf-8 -*-
# Macro d'affichage des pages perso 
# Update par Chirac : cette macro n'utilise plus le .info dans les home
# A la place, une table dans la base pg gérée par django coté intranet
# Désormais, seules les pages perso référencées dans la base sont affichées
# Le référencement constitue un choix volontaire, de l'utilisateur via l'intranet

import os
import psycopg2

class AccountList:
    home = "/home"
    www = "www"
    url = "http://perso.crans.org/%s/"

    def __init__(self):
        return

    def comptes(self):
        """Retourne la liste des comptes"""
#        return filter(lambda x: os.path.isdir(u"/home/%s/www" % x) and  not os.path.islink(u"/home/%s/www" % x),
#                      os.listdir(u"/home/mail"))
        ### ^^^^^^ le code m'a tuer, trace de mooo
        
        #Désormais, seuls les pages perso qui sont dans la base sont référencées.
        con = psycopg2.connect(database="django", user="crans", host="pgsql.adm.crans.org")
        cur = con.cursor()
        cur.execute("SELECT login FROM pageperso_pageperso")
        return cur.fetchall()


    def makeAnchor(self,letter):
        return u"<div class=\"vignetteperso\"><a class=\"letter_anchor\" name=\"index_%s\"><span>%s:</span></a></div>" % ( letter, letter )

    def makeIndex(self,letter_list):
        index = u''
        for aLetter in letter_list:
            index = u"%s<a href=\"#index_%s\">%s</a>" % ( index, aLetter, aLetter)
        return u"<div class=\"alphabetic_index\">%s</div>" % index

    def to_html(self):
        dirs = self.comptes()
        dirs.sort()
        html = u""

        premiere_lettre = ''
        letter_list = []
        for d in dirs:
            d=d[0]
            if premiere_lettre != d[0]:
                premiere_lettre = d[0]
                letter_list.append(premiere_lettre)
                html = u"%s\n%s" % ( html, self.makeAnchor(premiere_lettre) )
            html = u"%s\n%s" % (html, Account(self.home, d, self.www, self.url).to_html())
            
        index = self.makeIndex(letter_list)
        html = index + html
        html += u'<br style="clear: both">'
        return html

class Account:
    """Classe représentant la page perso d'une personne"""

    _connexion = None

    def __init__(self, home, login, www, url):
        """Instanciation avec le `login' de la personne"""
        self.login = login
        self.home = "%s/%s" % (home, login)
        self.www = www
        self.url = url

    _info = None
    def info(self, champ):
        """Retourne le contenu du champ `champ' dans le fichier info"""
        if self._info == None:

            self._info = dict()
            if not Account._connexion:
#                con = psycopg2.connect(database="intranet-dev", user="dev", password="verysecure", host="vo.v4.adm.crans.org",port="5432")
                con = psycopg2.connect(database="django", user="crans", host="pgsql.adm.crans.org") 
                Account._connexion = con
            else:
                con = Account._connexion
            cur = con.cursor()
            cur.execute("SELECT * FROM pageperso_pageperso WHERE login='%s' " % self.login )
            data = cur.fetchall()
            if data:   
                for indice,item in enumerate(data[0]):
                    self._info[cur.description[indice][0]]=item.decode('utf-8')


        if self._info.has_key(champ.lower()):
            return self._info[champ.lower()]
        else:
            return u""

    def chemin(self):
        """Chemin vers le www"""
        return os.path.join(self.home, self.www)

    def logo(self):
        """URL du logo s'il y en a un"""
        if self.info("logo"):
            # Le logo peut être en absolu ou en relatif
            if self.info("logo").startswith(self.chemin()):
                logo = self.info("logo").replace("%s/" % self.chemin(), "")
            else:
                logo = self.info("logo")
            if os.path.isfile(os.path.join(self.chemin(), logo)):
                return u"%s%s" % (self.url % self.login, logo)
        return u"http://perso.crans.org/pageperso.png"

    def to_html(self):
        """Renvoie le code HTML correspondant au fichier .info"""
        if self.login.lower() not in self.info("nom_site").lower():
            ident = u'%s<br>' % self.login
        else:
            ident = u''
        html = [ u'<div class="vignetteperso">',
                 u'<a href="%s">' % (self.url % self.login),
                 u'<img src="%s" alt="%s">' % (self.logo(), self.login),
                 u'</a><br>',
                 self.info("nom_site") and u'<b>%s</b><br>' % self.info("nom_site") or u'',
                 ident,
                 self.info("slogan") and u'<i>%s</i>' % self.info("slogan") or u'',
                 u'</div>' ]
        return u'\n'.join(html)


def execute(macro, args):
    return macro.formatter.rawHTML(AccountList().to_html())
