# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/secours')
import secours

def Cellule(texte, couleur, f) :
    """
    Retourne le code HTML d'une cellule formattée aver le formatter f
    """
    code  = f.table(1)
    code += f.table_row(1)
    code += f.table_cell(1,{'style':'background-color:%s; color: black;' % couleur })
    code += f.text(texte)
    code += f.table_cell(0)
    code += f.table_row(0)
    code += f.table(0)
    return code

def execute(macro, text) :
    try:
        f = open(secours.ETAT_MAITRE)
        if f.readline().strip() == 'normal':
            return Cellule('Nous sommes actuellement en connexion normale.','lime',macro.formatter)
        else :
            return Cellule('Nous sommes actuellement en connexion de secours.','red',macro.formatter)
    except :
        return Cellule('Impossible de déterminer l\'état de la connexion.','yellow',macro.formatter)
