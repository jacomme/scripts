#!/usr/bin/env python
# -*- coding: utf-8 -*-

""" Calcule l'âge en fonction de la date d'anniversaire (année, mois, jour)
    en utilisant la date actuelle. """

from MoinMoin.wikiutil import escape

import time
import datetime
import re

def execute(macro, text):
    year, month, day = text.split(",")
    y, m, d = int(year), int(month), int(day)
    now = datetime.datetime(*time.localtime()[:6])
    if (m, d) == (2, 29):
        birthdate = datetime.datetime(now.year, 3, 1)
        if (now.month, now.day) == (2, 29):
            now = datetime.datetime(now.year, 3, 1)
    else:
        birthdate = datetime.datetime(now.year, m, d)
    age = now.year - y
    if now < birthdate:
        age -= 1
    return macro.formatter.text(str(age))
