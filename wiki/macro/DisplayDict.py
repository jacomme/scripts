# -*- coding: utf-8 -*-
"""
    MoinMoin - ShowAcl macro

    @copyright: 2014 Vincent Le Gallic, Emmanuel Arrighi
    @license: GNU GPL v3

    Pour afficher un dictionnaire dans un tableau wiki.
"""

import sys
import os.path
import importlib

SECURE_PATHS = ['/usr/scripts']

def macro_DisplayDict(macro, args):
    """Suppose que args est de la forme ``path:variable_name``"""
    # Si on utilise une virgule, MoinMoin foire lamentablement… ("Too many arguments")
    fichier, variable = args.split(":")
    # On importe le fichier demandé
    path = os.path.dirname(fichier)
    path = os.path.realpath(path)

    def is_subdir(sec_path):
        """Renvoie ``True`` si path est bien un sous-dossier de ``sec_path``"""
        if not path.startswith(sec_path):
            return False
        if len(path) == len(sec_path):
            return True
        # Si path est strictement plus long, alors on doit s'assure qu'on a
        # bien un slash après notre préfixe ``sec_path``
        return path[len(sec_path)] == os.path.sep

    if not any( is_subdir(sec_path) for sec_path in SECURE_PATHS ):
        return """[[DisplayDict: forbidden]]"""
        
    if not path in sys.path:
        sys.path.append(path)
    
    filename = os.path.basename(fichier)
    filename = filename.rsplit(os.path.extsep, 1)[0]
    
    module = importlib.import_module(filename)
    # On recharge le module sinon on affichera toujours le contenu à la date
    # du dernier redémarrage du wiki
    module = reload(module)
    
    dict = module.__dict__[variable]
    keys = dict.keys()
    keys.sort()
    text = ""
    text += macro.formatter.table(1)
    for k  in keys:
        text += macro.formatter.table_row(1)
        text += macro.formatter.table_cell(1)
        text += macro.formatter.text(k)
        text += macro.formatter.table_cell(0)
        text += macro.formatter.table_cell(1)
        text += macro.formatter.text(dict[k])
        text += macro.formatter.table_cell(0)
        text += macro.formatter.table_row(0)
    text += macro.formatter.table(0)

    return text

