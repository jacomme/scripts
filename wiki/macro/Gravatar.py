# -*- coding: iso-8859-1 -*-
"""
    MoinMoin - Gravatar Macro
    Gregoire Detrez, 2007
    v 0.0.1
    You can include a gravatar image by using this macro
        [[Gravatar(name@exemple.com)]]
"""

def execute(macro, text):
    import md5
    parseargs = text.split(',')
    if len(parseargs)<2:
        return '<small><strong class="error">MailTo macro must have at least two arguments</strong></small>'
    emailname, emailurl= parseargs[:2]
    email = emailname.strip() + "@" + emailurl.strip()

    user_id = md5.md5( email.strip() ).hexdigest()
    url = 'http://www.gravatar.com/avatar.php?gravatar_id=%s&size=80' % user_id
    html = "<img src=\"%s\" alt=\"Gravatar\" />" % url
    return macro.formatter.rawHTML(html)
