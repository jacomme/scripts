#! /usr/bin/env python

months = { 'Jan': 1,
           'Feb': 2,
           'Mar': 3,
           'Apr': 4,
           'May': 5,
           'Jun': 6,
           'Jul': 7,
           'Aug': 8,
           'Sep': 9,
           'Oct': 10,
           'Nov': 11,
           'Dec': 12 }

import time

def syslog2pgsql(date):
    """Convertit une date du type `Jul 15 19:32:23' en une date du type `15-07-2005 19:32:23'"""
    mois = months[date[0:3]]
    jour = int(date[4:6])
    heure = date[7:]
    annee = time.gmtime()[0]
    return "%02d-%02d-%04d %s" % (jour, mois, annee, heure)
