-----------------------------------------------------------------
-- Maintenance de la base pgsql filtrage sur odlyd, lancé par cron
-----------------------------------------------------------------

-- on limite les risques de dérive en flushant régulièrement (une fois par jour)
-- la base de comptage
TRUNCATE accounting;
