#!/bin/bash

psql -d filtrage -f /usr/scripts/surveillance/maintenance/delete.sql
psql -d filtrage -f /usr/scripts/surveillance/maintenance/accounting.sql 2>&1 > /dev/null
/usr/scripts/surveillance/maintenance/check_activity.py
psql -d filtrage -f /usr/scripts/surveillance/maintenance/vacuum.sql 2>&1 > /dev/null
