To: roots@crans.org
From: %(fn)s <%(mail)s>
Subject: Oubli de mon mot de passe jabber

Bonjour,

Je me suis rendu sur https://jabber.crans.org/lostPassword.py
depuis %(host)s, car j'ai oublié le mot de passe de mon compte
jabber (%(user)s@jabber.crans.org).
Pourriez-vous m'en générer un nouveau et me l'envoyer par mail
à %(mail)s.

Merci bien,

-- 
généré par lostPassword.py

ps: ejabberd@xmpp$ ejabberdctl set-password %(user)s jabber.crans.org ${PASSWD}
