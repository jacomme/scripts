#!/bin/bash /usr/scripts/python.sh
# -!- encoding: utf-8 -!-
# SPAM_CABLAGES.PY -- Spamme respbats pour rappeler les cablages a effectuer

import psycopg2
from collections import defaultdict

import utils.sendmail

TEMPLATE = u"""
Câblages à effectuer :
%(cablages)s
--\u0020
L'intranet du Cr@ns"""

TEMPLATE_BATIMENT = u"""
Batiment %(batiment)s
%(prises)s"""

def get_cablages():
    """Recupere la liste des cablages a effectuer"""
    conn = psycopg2.connect("user=crans dbname=switchs host=pgsql.adm.crans.org")
    cur = conn.cursor()
    cur.execute("SELECT batiment, chambre, prise_crans FROM prises WHERE cablage_effectue = FALSE")
    res = defaultdict(list)
    for batiment, chambre, prise_crans in cur.fetchall():
        res[batiment].append(chambre)

    return res

CABLAGES = get_cablages()

if CABLAGES:
    CORPS_BATIMENTS = []
    for batiment in sorted(CABLAGES.keys()):
        ubatiment = batiment.upper()
        prises = ", ".join(["%s%s" % (ubatiment, chambre) for chambre in sorted(CABLAGES[batiment])])
        CORPS_BATIMENTS.append(TEMPLATE_BATIMENT % { "prises": prises, "batiment": ubatiment })
    
    message = TEMPLATE % { "cablages": "".join(CORPS_BATIMENTS) }
    if "--mail" in sys.argv:
        utils.sendmail.sendmail(u"intranet-bugreport@crans.org", u"respbats@lists.crans.org",
                                u"[Câblage] Résumé des câblages à effectuer", message, more_headers={u"X-Crans-Intranet" : u"cablage", u"X-Mailer" : u"/usr/scripts/spam_cablages.py"})
    else:
        print message
