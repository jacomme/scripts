# -*- coding: utf-8 -*-
from django.contrib.syndication.feeds import Feed
from email.Parser import Parser
import email.Utils 
import nntplib, datetime
from email.Header import decode_header
p = Parser()

def unicode_header( h ):
    h = decode_header( h )
    h = map(lambda x: x[0].decode(x[1] or "ascii"), h)
    return u" ".join( h )

class Message(object):
    def __init__(self, raw_message):
        ## parsing message
        emailMessage = p.parsestr(raw_message)
        ## getting subject right
        subject = emailMessage['Subject'].replace("\n", "")
        subject = unicode_header( subject )
        self.title = subject
        print subject
        ## getting body right
        body = None
        if emailMessage.is_multipart():
            body = "No text found"
            for part in emailMessage.get_payload():
                print part.get_content_type()
                if part.get_content_type() == "text/plain":
                    print  part.get_payload()
                    body = part.get_payload()
                    body = body.decode("quopri_codec")
                    body = body.decode(part.get_content_charset() or "ascii")
                    break      
        else:
            body = emailMessage.get_payload()
            body = body.decode("quopri_codec")
            body = body.decode(emailMessage.get_content_charset() or "ascii")
        
        self.body = body
        self.date = datetime.datetime( *email.Utils.parsedate( emailMessage.get('Date'))[:-2] )
        
    def get_absolute_url(self):
        return "http://www.crans.org"
    def __unicode__(self):
        return self.title

def getMessages(group, n=10):
    messages = []
    news = nntplib.NNTP('news.crans.org')
    resp, count, first, last, name = news.group(group)
    ## faire: renvoyer 404 si existe pas NNTPTemporaryError
    subs = news.xhdr('subject', str(int(last)-10) + '-' + last)[1]
    for id, sub in subs[-10:]:
        info, id, kd, article = news.article( id )
        msg = Message( u"\n".join(article) )
        messages.append( msg )
    messages.reverse()
    return messages

class DernieresAnnonces(Feed):
    title = "Annonces cr@ns"
    link = "/"
    description = "Annonces officielles de l'association cr@ns (Cachan Réseau @ Normal' Sup)"

    def items(self):
        return getMessages("crans.crans.annonces", 10)

    def item_pubdate(self, item):
        """
        Takes an item, as returned by items(), and returns the item's
        pubdate.
        """
        return item.date

class LatestMessages(Feed):
    title = "Annonces cr@ns"
    link = "/"
    description = "Annonces officielles de l'association cr@ns (Cachan Réseau @ Normal' Sup)"

    def get_object(self, bits):
        # In case of "/rss/beats/0613/foo/bar/baz/", or other such clutter,
        # check that bits has only one member.
        if len(bits) != 1:
            raise ObjectDoesNotExist
        return bits[0]
    def items( self, obj ):
        return getMessages(obj, 10)

    def item_pubdate(self, item):
        """
        Takes an item, as returned by items(), and returns the item's
        pubdate.
        """
        return item.date

