#! /usr/bin/env python
# -*- coding: iso-8859-15 -*-
"""chambre_on_off.py: Affiche le statut, active ou d�sactive une prise

 syntaxe : chambre-on-off.py chbre <cmd>
 chbre de la forme A101d
 <cmd> peut �tre :
   - on : activer la prise
   - off : d�sactiver la prise
   - absent : affiche le statut de la prise

"""

import sys
sys.path.append("/usr/scripts/gestion/")

import hptools

def chambre_on_off(chambre, cmd = None):
    """Active ou d�sactive une prise
    
    chambre: num�ro de chambre
    cmd: si fourni, "on" ou "off" pour activer ou d�sactiver la prise
    si cmd n'est pas fourni, ne fait qu'afficher le statut de la prise
    """
    # connexion au switch
    chbre_sw = hptools.sw_chbre(chambre)

    # affichage des infos
    print "chbre  : %s" % chambre
    print "switch : %s" % chbre_sw.switch
    print "prise  : %s" % chbre_sw.prise

    print "statut : %s" % (chbre_sw.is_enable() and "on" or "off")

    if cmd:
        cmd = cmd.lower()

        # modification du statut
        if cmd == "off":
            print "action : disable"
            chbre_sw.disable()
        elif cmd == "on":
            print "action : enable"
            chbre_sw.enable()

        # affichage du nouveau statut
        print "statut : %s" % (chbre_sw.is_enable() and "on" or "off")

if __name__ == '__main__':
    if len(sys.argv) == 2:
        chambre_on_off(sys.argv[1])
    elif len(sys.argv) == 3:
        chambre_on_off(sys.argv[1], sys.argv[2])
    else:
        print __doc__
        sys.exit(0)

