/* A compiler avec gcc -Wall -ansi -static addip.c -o /var/www/cgi-bin/addip.cgi */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main() {
	FILE *fd;
	char *ip;
	
	if ((fd = fopen("/hotspot.socket", "a")) == NULL)
         	goto erreur;
	
	if ((ip = getenv("REMOTE_ADDR")) == NULL)
	        goto erreur;
	
	fprintf(fd, "%s\n", ip);
	fflush(fd);
	fclose(fd);
	
	printf("Location: https://wifi.crans.org/wiki/WiFi(2f)PagesStatiques(2f)PortailCaptif(2f)AuthOk.html\r\n");
	printf("\r\n");
	return 0;
	
erreur:
	printf("Location: https://wifi.crans.org/wiki/WiFi(2f)PagesStatiques(2f)PortailCaptif(2f)AuthErreur.html\r\n");
	printf("\r\n");
	return 0;
}
