#!/usr/bin/python
# -*- coding: iso-8859-1 -*-
"""
Module BuildDb.py

Ce module regarde les repertoire dans le home, puis
interroge chaque compte pour lire le .info, le .plan etc...
"""

import os
from string import *
import CompteRec

def GrabInfos(aDB):
    """ Liste le contenu de /home .
    
    le resultat est stocke dans le CInfoDb fourni en argument (aDB)
    """
    
    # Pour debug : on peut ne tester que i comptes
    i = -1

    maison = os.listdir('/home')

    for login in maison:
        compte = CompteRec.CCompte(login)

        if compte: # il y a un .info, un .plan ou un .www :
            aDB.Put(login,compte)
        
        i = i - 1
        if not i: break  # en debogage, on ne mouline pas tous les comptes !
        



########################
# debug :

if 0:
    import InfoDb
    
    GrabInfos(InfoDb.CInfoDb("/tmp/test","n"))
