# Sourcé par python.sh en environnement de test, à modifier suivant vos envies
# (et les tests que vous souhaitez faire)
#
# Pour ldap et la base postgres: il est possible de forwarder les connexions
# vers une base distante (celle de vo) pour éviter d'avoir à en configurer
# une locale. Les exemples donnés (en commentaire) ci-dessous permettent
# de se connecter avec le forward ssh suivant:
# $ ssh vo.crans.org -L 3899:localhost:389 -L 5432:localhost:5432

# Utiliser ldap local
export DBG_LDAP=1
# export DBG_LDAP=localhost:3899
# ou 1 pour localhost

# Utiliser l'annuaire pgsql local
export DBG_ANNUAIRE=1
# export DBG_ANNUAIRE=localhost
# ne pas indiquer de port ici (pas supporté)
# ou 1 pour localhost

# Trigger est-il en mode débug ?
export DBG_TRIGGER=1

# Mails auto, plusieurs valeurs:
# * print: affiche le mail au lieu de l'envoyer
# * une adresse mail: envoie tous les mails à cette adresse mail au lieu de
#   la vraie
# NB: noter que pour le moment, cela ne marche pas avec tous les scripts.
# Attention à ne pas envoyer de mails aux adhérents par erreurs !
export DBG_MAIL=`whoami`+test@crans.org

# clogger produit des fichiers de logs
export DBG_CLOGGER_PATH=/tmp/`whoami`/clogs
mkdir -p $DBG_CLOGGER_PATH

# Serveur freeradius de test ?
export DBG_FREERADIUS=1

# Imprime pour de vrai ?
export DBG_PRINTER=1

# Un dossier où trouver une version alternative des secrets (fichier par
# fichier)
export DBG_SECRETS=/etc/crans/dbg_secrets/`whoami`/

# Pour la wifimap
export DBG_WIFIMAP_DB=$CPATH/var/wifi_xml
