#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Copyright (C) 2011 Michel Blockelet
# Licence : GPLv3

u"""Ce script permet aux trésoriers d'effectuer les contrôles en spécifiant
chaque adhérent par une expression de recherche et les contrôles à effectuer
sur cette personne.

Exemple :
 controle_tresorier3.py nom=blockelet+pc 2190c
"""

import sys
sys.path.append('/usr/scripts/gestion')
from config import ann_scol
from ldap_crans import crans_ldap
from affich_tools import coul

db = crans_ldap()

if __name__ == "__main__":
    if len(sys.argv) > 1:
        # Aide
        if sys.argv[1] == '-h':
            print __doc__
            sys.exit(1)

        print coul('=== Mode non-interactif ===', 'vert')
        for act in sys.argv[1:]:
            # Deux formats
            if '+' in act:
                # [recherche]+[contrôle] : nom=blockelet+pc
                [sexpr, contr] = act.split('+')
            else:
                # [aid][contrôle] : 1240pc
                contr = ('p' if 'p' in act else '') + ('c' if 'c' in act else '')
                sexpr = "aid=%s" % act.replace('p', '').replace('c', '')

            # Recherche
            try:
                adhl = db.search(sexpr, 'w')['adherent']
            except:
                print coul(" *** Erreur de recherche : (%s)(+%s)" %
                    (sexpr, contr), 'rouge')
                continue

            if len(adhl) == 0:
                print coul(" *** Aucun adhérent trouvé : (%s)(+%s)" %
                    (sexpr, contr), 'rouge')
                continue
            if len(adhl) > 1:
                print coul(" *** Plusieurs adhérents trouvés : (%s)(+%s)" %
                    (sexpr, contr), 'rouge')
                continue
            adh = adhl[0]

            modif = False

            # On regarde les contrôles à faire
            newcontr = contr
            if 'p' in adh.controle():
                newcontr = newcontr.replace('p', '')
            if 'c' in adh.controle():
                newcontr = newcontr.replace('c', '')

            try:
                print coul(" * %s (aid=%s) : %s +%s -> %s" % (adh.Nom(),
                    adh.id(), adh.controle(), contr, (newcontr if newcontr else
                    '[rien]')), 'bleu')
            except:
                print coul(" * [Nom non imprimable] (aid=%s) : %s +%s -> %s" %
                    (adh.id(), adh.controle(), contr, (newcontr if newcontr
                    else '[rien]')), 'bleu')

            # Si la carte n'est pas notée, on le fait
            if not adh.carteEtudiant() and 'c' in contr:
                print coul("Ajout de la carte", 'violet')
                adh.carteEtudiant(ann_scol)
                modif = True

            # On fait les contrôles
            if modif or newcontr != '':
                if newcontr == 'pc' or newcontr == 'cp':
                    adh.controle('+p')
                    adh.controle('+c')
                else:
                    adh.controle('+%s' % newcontr)
                adh.save()
            else:
                print coul("Aucune modification", 'jaune')
