# -*- coding: utf-8 -*-

class PagePerso:
    """Classe représentant la page perso d'une personne"""
    
    home = "/home"
    www = "/www"
    
    def __init__(self, login):
        """Instanciation avec le login' de la personne"""
        self.login = login
        self.home = "%s/%s" % (self.home, login)
        _info = {}
        self._load_informations()

    def _filename( self ):
        return "%s/.info" % self.home
    
    def _load_informations(self):
        try:                                                                                                                  
            lignes = file( self._filename() )
        except IOError:                                                                                                       
            lignes = []
        # self._info est un dictionnaire qui reprend le contenu du .info                                                      
        self._info = dict( map( lambda z: (unicode(z[0].lower(),"iso-8859-15"),                                                 
        unicode(z[1],"iso-8859-15")),                                                                                         
        filter(lambda w: len(w) == 2 and len(w[1]),                                                                           
        map(lambda x: map(lambda y: y.strip(),                                                                                
        x.split(":")),
        lignes))))
        
    def _save_informations( self ):
        myfile = file(self._filename(), "w") 
        for aKey in self._info.keys():
            myfile.write("%s:%s\n" % (aKey, self.
            _info[aKey]) )
        myfile.write("\n")

    def save( self ):
        self._save_informations()
        
    def chemin(self):
        """Chemin vers le www"""
        return u"%s%s" % (self.home, self.www)
    
    def url(self):
        """URL vers la page perso"""
        return u"http://perso.crans.org/%s/" % self.login
    
    def nom( self ):
        return self._info.get("nom", "")
    
    def setNom( self, nom ):
        self._info["nom"] = nom
        
    def slogan( self ):
        return self._info.get("slogan", "")
    
    def setSlogan( self, slogan ):
        self._info["slogan"] = slogan
    
    def logo(self):
        """URL du logo s'il y en a un"""
        logo = self._info.get("logo", None)
        if logo:
            # Le logo peut être en absolu ou en relatif
            if logo.startswith(self.chemin()):
                logo = logo.replace("%s/" % self.chemin(), "")
            if os.path.isfile("%s/%s" % (self.chemin(), logo)):
                return u"%s%s" % (self.url(), logo)
        return u"http://perso.crans.org/pageperso.png"
                                                                                                                                
    def __str__(self):
        """Renvoie le code HTML correspondant au fichier .info"""
        html = [ u'<div class="vignetteperso">',
        u'<a href="%s">' % self.url(),
        u'<img src="%s" alt="%s">' % (self.logo(), self.login),
        u'</a><br>',
        self.info("nom") and u'<b>%s</b><br>' % self.info("nom") or u'%s<br>' % self.login,
        self.info("devise") and u'<i>%s</i>' % self.info("devise") or u'',
        u'</div>' ]
        return u'\n'.join(html)

