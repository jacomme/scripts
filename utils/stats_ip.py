#! /usr/bin/env python
# -*- coding: utf-8 -*-

import sys
sys.path.append('/usr/scripts/gestion')

from ldap_crans import crans_ldap
from config import NETs
from iptools import AddrInNet

ips = [ x.ip() for x in crans_ldap().search('ip=*')['machine'] ]

for n in NETs:
    
    nb_total = 0
    for net in NETs[n]:
        nb_total += 2 ** ( 32 - int( net.split('/')[1] ) )
    
    nb_occupe = len( [ ip for ip in ips if AddrInNet( ip, NETs[n] ) ] )
    
    print '%12s %2s%% (%s/%s)' % (n, 100*nb_occupe/nb_total, nb_occupe, nb_total)
