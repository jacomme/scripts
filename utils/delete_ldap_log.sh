#!/bin/bash
# Petit script pour réduire la place prise par les anciens logs 
# de ldap qui ne sont plus utilisés

STORE=/var/lib/ldap
ARCHIVE_BIN=/usr/bin/db5.1_archive

for f in `$ARCHIVE_BIN -h $STORE`; do
    echo Removing $f ...
    rm -f $STORE/$f 
done

